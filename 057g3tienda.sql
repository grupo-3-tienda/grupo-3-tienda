-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-09-2022 a las 23:51:44
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `057g3tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `idcategoria` varchar(10) NOT NULL COMMENT 'Identificacion categoria del producto\r\n',
  `descripcion` varchar(70) NOT NULL COMMENT 'Descripcion categoria del producto\r\n',
  `estado` char(1) NOT NULL COMMENT 'Estado de categoria (Activo - Inactivo)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE `detalle_venta` (
  `iddetalle_venta` int(10) NOT NULL COMMENT 'Identificador detalle venta',
  `idventa` int(10) NOT NULL COMMENT 'Identificador de la venta',
  `idproducto` varchar(10) NOT NULL COMMENT 'Identificador del producto',
  `cantidad` int(4) NOT NULL COMMENT 'Cantidad de producto',
  `precio` float NOT NULL COMMENT 'Valor precio unitario del producto',
  `valor` float NOT NULL COMMENT 'Valor total de la venta del producto (Cantidad * Precio)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `id_marca` varchar(10) NOT NULL COMMENT 'Identificador de la marca\r\n',
  `descripcion` varchar(60) NOT NULL COMMENT 'Descripcion de la marca',
  `estado` char(1) NOT NULL COMMENT 'Estado de la marca (Activo - Inactivo)\r\n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `idpermiso` int(1) NOT NULL COMMENT 'Identificador del permiso',
  `descripcion` varchar(60) NOT NULL COMMENT 'Descripcion del permiso(Administrador-Empleado)',
  `estado` char(1) NOT NULL COMMENT 'Estado del permiso (Activo-Inactivo)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idproducto` varchar(10) NOT NULL COMMENT 'Identificador del producto',
  `idcategoria` varchar(10) NOT NULL COMMENT 'Identificador de la categoria',
  `precio_compra` float NOT NULL COMMENT 'Valor del precio de la compra',
  `precio_venta` float NOT NULL COMMENT 'Valor del precio de la venta',
  `stock` int(2) NOT NULL COMMENT 'Cantidad stock minimo de la venta',
  `id_marca` varchar(10) NOT NULL COMMENT 'Identificador de la marca',
  `descripcion` varchar(70) NOT NULL COMMENT 'Descripcion del producto\r\n',
  `fecha_vigencia` date NOT NULL COMMENT 'Fecha de vigencia del producto\r\n',
  `estado` char(1) NOT NULL COMMENT 'Estado del producto (Activo - Inactivo)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(10) NOT NULL COMMENT 'Identificador del usuario',
  `idpermiso` int(1) NOT NULL COMMENT 'Identificador del permiso',
  `nombre` varchar(60) NOT NULL COMMENT 'Nombre completo del usuario',
  `direccion` varchar(60) NOT NULL COMMENT 'Direccion de residencia del usuario',
  `telefono` varchar(20) NOT NULL COMMENT 'Telefono o celular del usuario',
  `correo` varchar(50) NOT NULL COMMENT 'Correo Electronico del usuario',
  `clave` varchar(4) NOT NULL COMMENT 'Clave de ingreso del usuario',
  `estado` char(1) NOT NULL COMMENT 'Estado del usuario (Activo - Inactivo)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idventa` int(10) NOT NULL COMMENT 'Identificador de la venta',
  `fecha` date NOT NULL COMMENT 'Fecha de registro de la venta',
  `valor` float NOT NULL COMMENT 'Valor total de la venta',
  `id_usuario` int(10) NOT NULL COMMENT 'Identificar del usuari oque registro la venta',
  `estado` char(1) NOT NULL COMMENT 'Estado de la venta(Activo-Inactivo)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indices de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD PRIMARY KEY (`iddetalle_venta`),
  ADD KEY `fkproducto` (`idproducto`),
  ADD KEY `fkventa` (`idventa`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id_marca`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`idpermiso`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idproducto`),
  ADD KEY `fkcategorias` (`idcategoria`),
  ADD KEY `fkmarca` (`id_marca`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `fkpermiso` (`idpermiso`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idventa`),
  ADD KEY `fk_usuario` (`id_usuario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD CONSTRAINT `fkproducto` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkventa` FOREIGN KEY (`idventa`) REFERENCES `venta` (`idventa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fkcategorias` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkmarca` FOREIGN KEY (`id_marca`) REFERENCES `marca` (`id_marca`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fkpermiso` FOREIGN KEY (`idpermiso`) REFERENCES `permiso` (`idpermiso`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `fk_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
